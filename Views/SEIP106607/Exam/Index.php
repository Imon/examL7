<?php
use App\BITM\SEIP106607\Exam\Exam;
include_once ("../../../vendor/autoload.php");


$Total_Info = new Exam();
$Information = $Total_Info ->Index();

?>

<!DOCTYPE html>
<html>
    <head>
        <title>All the Information is here</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <h1>Personel details</h1>
    <table border="1">
        <thead>
            <td>SL</td>
                <td>Name</td>
                <td>Profession</td>
                <td colspan="3">Action</td>
        </thead>
        <tbody>
        <?php foreach ($Information as $info):?>
                <tr>
                    
                    <td><?php echo $info['id'];?></td>
                    <td><?php echo $info['name'];?></td>
                    <td><?php echo $info['profession'];?></td>
                    <td><a href="index.php">View</a></td>
                    <td><a href="create.php">Edit</a></td>
                    <td><a href="#">Delete</a></td>
                </tr>
                <?php endforeach; ?>
        </tbody>
    </table>
    </br>
    <h1><a href="create.php">Create New</a></h1>
    <h1><a href="../../../index.php">Back To the Home</a></h1>
    </body>
</html>